scores = [83, 71, 92, 64, 98, 87, 75, 69]
high_scores = scores.select { |s| s > 80 }
puts "High scores:"
puts high_scores
puts

low_scores = scores.reject { |s| s > 80 }
puts "Low scores:"
puts low_scores
puts

puts "Failing:"
puts scores.detect { |s| s < 70 }

scores_doubled = scores.collect { |s| s * 2}

puts "\nScores Doubled:"
p scores_doubled

total = scores.reduce(0, :+)
puts "\nScores Total:"
p total
