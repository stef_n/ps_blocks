1.upto(3) { |n| puts "#{n} Echo!"}
puts

# 5.times do
#   puts "situp\npushup\nchinup"
# end

1.upto(5) do |n|
  puts "#{n} situp"
  puts "#{n} pushup"
  puts "#{n} chinups\n\n"
end
