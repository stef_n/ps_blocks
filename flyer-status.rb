class Flyer
  attr_reader :name, :email, :miles_flown
  attr_accessor :status

  def initialize(name, email, miles_flown, status=:bronze)
    @name = name
    @email = email
    @miles_flown = miles_flown
    @status = status
  end

  def to_s
    "#{name} (#{email}): #{miles_flown} - #{status}"
  end
end

flyers = []
flyers << Flyer.new("Larry", "larry@example.com", 4000, :platinum)
flyers << Flyer.new("Moe", "moe@example.com", 1000)
flyers << Flyer.new("Curly", "curly@example.com", 3000, :gold)
flyers << Flyer.new("Shemp", "shemp@example.com", 2000)


frequent_flyers = flyers.select { |f| f.miles_flown >= 3000 }
puts frequent_flyers
puts

infrequent_flyers = flyers.select { |f| f.miles_flown <= 3000 }
puts infrequent_flyers

puts "Platinum Status:"
puts flyers.any? { |f| f.status == :platinum }
puts

puts "Bronze status flyer to call:"
first_bronze_status = flyers.detect { |f| f.status == :bronze}
puts first_bronze_status

platinum_flyers, other_flyers = flyers.partition { |flyer| flyer.status == :platinum }

puts "\nPlatinum Flyers:"
puts platinum_flyers

puts "\nOther Flyers:"
puts other_flyers

name_tags = flyers.map { |flyer| "#{flyer.name} (#{flyer.status.upcase})" }
p name_tags

total_kms = flyers.map { |flyer| flyer.miles_flown * 1.6 }
puts "\nLufthansa Report (km):"
p total_kms

total_miles_flown = flyers.reduce(0) { |sum, flyer| sum + flyer.miles_flown }
puts "\nTotal Miles:"
p total_miles_flown

total_kms_flown = flyers.map { |flyer| flyer.miles_flown * 1.6 }.reduce(0, :+)
puts "\nTotal km flown: #{total_kms_flown.to_i}"
