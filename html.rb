def tag(name)
  print "<#{name}>\n"
  print yield
  print "\n</#{name}>\n"
end

tag(:h1) { "Breaking News!" }
tag(:h2) { "Massive Ruby Discovered" }
tag(:ul) do
  tag(:li) { "It sparkles!"}
  tag(:li) { "It shines!"}
  tag(:li) { "It mesmerizes!"}
end
