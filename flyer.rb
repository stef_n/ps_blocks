class Flyer
  attr_reader :name, :email, :miles_flown, :total

  def initialize(name, email, miles_flown)
    @name = name
    @email = email
    @miles_flown = miles_flown
  end

  def to_s
    "#{name} (#{email}): #{miles_flown}"
  end
end

  flyers = []
  1.upto(5) do |n|
  flyers << Flyer.new("Flyer #{n}", "flyer#{n}@example.com", n * 1000)
  end

  flyers.each { |f| puts "#{f.name} - #{f.miles_flown} miles"}

  total = 0
  flyers.each { |f| total += f.miles_flown }
  puts
  puts "Total miles flown: #{total}"
  puts

  promotions = { "United" => 1.5, "Delta" => 2.0, "Lufthansa" => 2.5 }
  promotions.each { |airline, reward| puts "Earn #{reward}x miles by flying #{airline}" }
