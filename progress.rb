def progress
  0.step(100, 10) do |number|
    yield number
  end
end

progress { |percent| puts percent}
