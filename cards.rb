cards = ["Jack", "Queen", "King", "Ace", "Joker"]

cards.shuffle.reverse_each { |c| puts "#{c.upcase} - #{c.length}"}

scores = {"Larry" => 10, "Moe" => 8, "Curly" => 12}

scores.each { |n, s| puts "#{n} scored a #{s}!"}
