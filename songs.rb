require_relative 'my_enumerable'
class Song
  attr_reader :name, :artist, :duration

  def initialize(name, artist, duration)
    @name = name
    @artist = artist
    @duration = duration
  end

  def play
    puts "Playing '#{name}' by #{artist} (#{duration})..."
  end

  def each_filename
    basename = "#{name}-#{artists}".gsub(" ", "-").downcase
    extensions = [".mp3", ".wav", ".aac"]
    extensions.each { |ext| yield basename + ext }
  end
end

song1 = Song.new("Bad Blood", "Taylor Swift", 3)
song2 = Song.new("Footsteps", "Pop Evil", 4)
song3 = Song.new("The Wicked Ones", "Dorothy", 2)
song4 = Song.new("Wipe Yourself Off, Man. You Dead.", "Four Year Strong", 3)

class Playlist
  # include Enumerable
  include MyEnumerable
  def initialize(name)
    @name = name
    @songs = []
  end

  def add_song(song)
    @songs << song
  end

  def each
    @songs.each { |song| yield song }
  end

  def play_songs
    each { |song| song.play }
  end

  def each_tagline
    @songs.each { |song| yield "#{song.name} - #{song.artist}" }
  end

  def each_by_artist(artist)
    select { |song| song.artist == artist}.each { |song| yield song}
  end
end

playlist = Playlist.new("Summer Jams")
playlist.add_song(song1)
playlist.add_song(song2)
playlist.add_song(song3)
playlist.add_song(song4)

playlist.each { |song| song.play }
puts
playlist.play_songs

wicked_songs = playlist.my_select { |song| song.name =~ /Wicked/ }
puts "\nHere are the wicked songs: \n#{wicked_songs}"

non_wicked_songs = playlist.my_reject { |song| song.name =~ /Wicked/ }
puts "\nHere are the not so wicked songs: \n#{non_wicked_songs}\n\n"

p playlist.my_any? { |song| song.artist == "Taylor Swift" }
p playlist.my_detect { |song| song.artist == "Taylor Swift" }

song_labels = playlist.my_map { |song| "#{song.name} - #{song.artist}" }
puts "\nHere is the playlist:\n#{song_labels}"

total_duration = playlist.my_reduce(0) { |sum, song| sum + song.duration }
puts "It will run for around #{total_duration} mins..."
